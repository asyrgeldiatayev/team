# Math Tutor Bot

## Example Problems

Find math word problems comparable to those taught at the UCSD math 20 series courses:

- [20a](http://math.ucsd.edu/~jeggers/math20a/)
- [20b](http://www.math.ucsd.edu/~jeggers/math20b/)
- [20c](http://www.math.ucsd.edu/~jeggers/math20c/)

## Example Solutions

Solve these word problems and record your solutions in a structured format like the yaml example here:

```yaml
-
	Question: A train leaves New York City at Noon on Christmas Eve, heading to Portland, Oregon at an average speed of 50 miles per hour. Another train leaves Portland at the same time heading to NYC at a constant speed of 100 miles per hour. Both trains are on the same track that is 2000 miles long and has a speed limit of 100 miles per hour. Neither trains never stop. And they never speed up or slow down by more than 10 miles per hour. Where do the trains meet and crash into each other if they do not switch tracks?
	Solution:
		1: Compute the speed of both trains combined since that's the rate at which they are converging on the track (130 mph) 
		2: Compute the time of impact or crossing of the two trains by dividing the track length by the speed the trains are converging 2,100 miles / 150 mph is 14 hours after departure.
		3: Compute the distance traveled by the train leaving NYC over 14 hours: 14 hr * 50 mph is 700 miles
		4: Compute the distance traveled by the train leaving Portland over 14 hours: 14 hr * 100 mph is 1,400 miles
		5: 1400 + 700 = 2100 miles so both answers are correct: the trains will meet 700 miles west of NYC and 1400 miles east of Portland at 2 am on Christmas morning Eastern time and 11 pm Christmas eve Pacific time.
	Bonus: Notice that the "noon" departure time is ambiguous and could mean Noon Eastern time or UTC time. And the "same time" could mean Noon Pacific time, Noon Eastern time, or Noon Pacific. If Both times are indeed calculated in the same time zone then the answer given in the Solution is correct. But if the New York departure time is Noon Eastern, and the Portland departure is None Pacific, then the New York train leaves 3 hours before the Portland train so the distance of track they travel together should be reduced by 3 hr times 50 miles per hour (150 miles) causing the impact point to shift 75 miles to the West and happen 1 hour later, at 3 am instead of 2 am on Christmas morning (Eastern time).
-  
```

## Existing SOTA

Play with Chegg's math tutor bot by inputing the problems from your benchmark testset. 
Record the questions and answers (and any hints or step-by-step instructions) in a structured data file such as YAML something like this:

### Problem Solvers

#### Recently updated Open source Python code

1. [MWPToolkit: An Open-Source Framework for Deep Learning-Based Math Word Problem Solvers](https://paperswithcode.com/paper/mwptoolkit-an-open-source-framework-for-deep) -- paperswithcode
  - [AAAI 2022 demo-track paper](https://arxiv.org/pdf/2109.00799v2.pdf)
  - [Python code](https://github.com/lyh-yf/mwptoolkit)
  - [consolidated/aggregated datasets](https://github.com/LYH-YF/MWPToolkit/tree/master/dataset) - SVAMP, alg514, APE (ape200k), asdiv-a, dolphin1878, draw, ept, hmwp, math23k, MAWPS-single, MAWPS (with redundant asdiv-a removed)
  - compared BERT-TD and MWP-BERT set2tree models to LSTMs, GRUs, etc
2. [Goal selection and feedback for solving math word problems](https://link.springer.com/content/pdf/10.1007/s10489-022-04253-1.pdf)
  - [SCNU203/GSFSF](https://github.com/SCNU203/GSFSF) - 2022 Python code trained on APE and updated 2022
  - [MAWPS - math word problem repository](https://github.com/sroy9/mawps) - java web server code, no data 
  - [MAWPS/ACE paper](http://sweaglesw.org/linguistics/ace/)
  - [APE dataset of 210k generated examples from 60k problem templates](http://sweaglesw.org/linguistics/ace/)
3. [Math Word Problem Solving with Explicit Numerical Values](https://aclanthology.org/2021.acl-long.455.pdf)
  - [NumS2T Python code](https://github.com/qinzhuowu/NumS2T)

#### paywalled/proprietary

- [Classifying and Solving Arithmetic Math Word Problems—An Intelligent Math Solver](https://ieeexplore.ieee.org/abstract/document/9350233)
- Wolfram Alpha

### Tutoring webapps, teaching systems

- [Mathematics Word Problems Solving by English Language Learners and Web Based Tutoring System](https://repository.arizona.edu/bitstream/handle/10150/193243/azu_etd_10823_sip1_m.pdf?sequence=1) - just figured out how to detect common English errors and track progress/mastery of English+Algebra
