# Augmented Analytics

Augmented Analytics is a business intelligence buzword.
Gartner predicted it would be dominant in 2021.


- "cognitive analytics"
- "analytics 2.0"
- "automated analytics"
- "automated data science"

Random thoughts and links on augmented analytics and automated data science below.

## overview

[free code camp generic overview](https://www.freecodecamp.org/news/what-is-augmented-analytics-definition-example/)
[github automated analytics python](https://github.com/search?q=automated+analytics+python)

## Automated DS
- Data Robot
- AutoML

## ancient History

### NLG
- weather radio shortwave
- MSNBC news financial trends
- traders
- baseball playby play

### auto etl (personal history)
- Sharp Labs mining technician repair reports and database with 100+ tables
- Building Energy (power bills, and building eqipmemt lists)
- talentpair scraped resumes   and jobs
- Tangible AI and Bottom Line: mining college advisor records

## nlu & knowlege management
- social media sentiment analysis for nonprofit branding ( donors and outreach impact assessment)
- [qary](docs.qary.ai)
- paperswithcode.com automates a lot of their analysis, etc
- customer service (chatbots augmented by humans)
- personal Google (personal.AI)
- question answering on text
- meilisearch
- elastic search
- old Google desktop search


### osint (Open source intelligence)
- Reddit Detective
- https://github.com/jivoi/awesome-osint
- Deep fake debunking
- Image search (scraping Instagram and collecting semi-public images to track people)
- cybersecurtity/ethical-hacking: spiderfoot
- cybersecurtity/ethical-hacking: holehe
- PIPL
- https://haveibeenpwned.com/

## pitfalls/education
- http://www.tylervigen.com/spurious-correlations
- Book of Why by Judea Pearl
- paperswithcode.com


## dashboard platforms claiming the buzz word
- graphana
- power bi
- tableau
- data studio

## open source projects

OS tools tend to do it better than commercial projects

- Autocomplete
- essentially recommendation engines for data/insights
- auto-etl

- Superset




### server analytics
- new relic
- datadog
- graphene
- git guardian
- code health
- search for best practices

Basics
- linters with cyclomatic complexity
- codecov
