# Python GIS

- [folium](http://python-visualization.github.io/folium/) integrates nicely with Python datasets
- [plotly maps](https://plotly.com/python/maps/) are interactive and render to SVG and HTML for integration into any website