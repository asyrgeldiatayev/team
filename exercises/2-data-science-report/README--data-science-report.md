# Data Science Report

Your final report should be viewable in GitLab and editable/resuable by others.
The best file format for this is markdown (`.md`).

Here are the elements you need for a data science project report.

## Summary (Introduction)

the summary of yoru project should include a user story where you describe a successful use of your application by a user.

For example, Camille and Bhanu's summarizers might go something like this:

> "A student runs a python function on a file path. The function converts the PDF into text and returns 3 sentences that summarize the entire document."

For a chatbot this is called the "happy path" when it describes several statements by the chatbot and the user:

> "A student runs `qary -s summarize` and pastes the URL of a web page into the command line: `YOU: summarize http://en.wikipedia.org/wiki/natural_language`. Qary responds with a list of topics found in the document, such as `BOT: 1) science, 2) technology, 3) NLP, 4) ethics`. User selects a topic from the list with `YOU: nlp`. 

Think about other users and stakeholders that might have an interest in helping you design your final product. In the case of a teaching product, think about what the school administrators, parents, and teachers would think of your product. And is it helping students learn? Or is it a crutch that prevents them from learning the material that the summarizer provides them with.

So you might describe the stakeholders of a summarizing chatbot with something like this.

> "Businesses want to help their employees find and understand the documentation they need. Customers want to find the right section of the documentation to read to help them with their problem.Think about the different people that might have an interest in your project and might want to use it. Managers, users, developers, etc

## ETL: Where did you get the data

## EDA: Visualizations and what they mean to you

## Modeling: 

Describe the model architecture that you would recommend to your stakeholders or the users of the model.
Make some example predictions that are correct and some that are incorrect to help your reader understand what your model can do well, and what it can't.
Summarize the model performance metrics for both your training set and test set or validation set, such as:

#### Classifier metrics
- accuracy
- precision
- recall
- F1-score
- a confusion matrix

#### Regression metrics
- standard error (RMSE)
- mean or median absolute error (MAE)
- mean or median absolute precision (MAP)
- correlation score (R value)

#### Other performance metrics
- RAM required to train
- RAM required for inference (prediction)
- training time
- inference (`.predict()`) time
- data efficiency (number of labeled data points)


### Hyperparameter table

A hyperparameter table lists all the experiments that you ran and how well they worked out.
Each time you train a model and test it on your test set, it's an experiment.
And you want to see how well each model predicted the target variable on your test set.
Make sure the table contains all the information you would need in order to retrain the model and achieve the same results.
Your experiments need to be reproducible, or they don't "count".
If you can't reproduce your results then you won't be able to retrain and reuse your model architecture in the real world on real data.
There's nothing worse than searching for days to find just the right combination of hyperparameters and then losing it.

A hyperparameter table is the best way to show your reader all the things you did to tune the model.
And it's a great way to review with yourself all the things you learned about your problem.
It also helps you justify why you architected the pipeline the way you did.
And sometimes your future-self is your best source of ideas on how to improve on what you've done, or do something different.

Here's an example data structure I use to record my experiments:

```python
experiments = []
experiment = dict(
    model='LogisticRegression',
    stop_words=False,
    C=.125,
    f1_test=0.84,
    f1_train=0.42,
    )
experiments.append(experiment)
df_experiments = pd.DataFrame(experiments)
df_experiments.sort_values('f1_test')
```


## Results: MVP Minimum Viable Product

What were you able to deliver to your "users".
You can say you succeeded if you are able to deliver a minimally useful software package, even if you are the only one who can use it.

Deliverable: screenshots, documentation, and or a video of your deliverable product and how well it works.
Tell people how accurate your model is and  what people can do with it.

You can also talk about any insights you gleaned from your model.
`Lasso|Linear|LogisticRegression.coef_` is usually a motherload of interesting information about which of your features are correlated with your target variable.
Even if you best model was not a linear or logistic regression, you probably want to train one so you can mine your model for insights.
The relationships between your features and the target variable can sometimes give you ideas about how your stakeholders might change something in the real world to try to influence the value of the target variable.
These are called "business-actionable insights."
If you can come up with just one useful bit of insight about your data or your problem, that's a big success for most projects.

## References and Tools

- `sklearn` Pipeline - data structures for parameterizing models (`dict`) and automating hyperparameter tuning 
- [`hypertable.py`](./../../exercises/1-python-intro/utils/hypertable.py) - my tools for appending to a hyperparameter table on disk
