# Job search data

## REST APIs

A REST API will allow you to retrieve data (usually JSON) from a web endpoint (a server at a specific URL.

Adzuna is a job listing site in the United Kingdom.
They have a free REST API you can use to search their database and retrieve structured data about jobs: 

* [developer.adzuna.com/overview](https://developer.adzuna.com/overview)
* [api.adzuna.com/v1/api](https://api.adzuna.com/v1/api)

You don't have to use the Adzuna API.
Alternatives include `indeed.com` and `upwork`.
Feel free to use any resource you have that can give you a list of at least 100 job postings (job descriptions posted by hiring managers).
As long as you can create structured data out of the text you download, that will be enough to complete this exercise you can use that data for this exercise.


## Exercise

- Read the API overview or getting started guide at any job search API service. Adzuna is at [developer.adzuna.com/overview](https://developer.adzuna.com/overview)
- Create a free account on the API you want to use (username and password)
- Use your username and password (account) to create an API Token or Key
- Use the API and the Python `requests` package or a command line tool like `curl` or `wget` to retrieve information about a Job posting or Company in `.json` format
- Use the API to retrieve 100 job postings and save the data to a YAML or JSON file
- Create a pandas DataFrame with columns for the job title, company name, job description, and any other data you think would be interesting.
- Populate your dataframe with the 100 job postings
- Create a brute force full text search algorithm that can find a substring in any of the descriptions in your dataframe of data
- Create a `if __name__ == "__main__":` section that can accomplish that search from the command line
- See if you can speed up the 
