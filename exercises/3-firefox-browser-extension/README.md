# LibreWolf (Firefox) Web Browser Extension Tutorial

Here's the 1 hr ["exercise"](https://gitlab.com/tangibleai/team/-/tree/main/exercises/3-web-browser-extension/README.md) on Tangible AI's team repo.

It's one of the tasks for the Supbase Bookmarking Extension project in the database of [intern project ideas](https://gitlab.com/tangibleai/team/-/tree/main/exercises/2-plan-your-prosocial-ai-project/prosocial-ai-project-ideas-plan-your-project.yml).

This is just a dumb copy of the Build [your first Web Browser Extension](https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/Your_first_WebExtension) tutorial.

It took me about an hour to get the json syntax right.

Be sure to set your unique app ID so you can share it with others to install in their browsers as a zip (https://extensionworkshop.com/documentation/develop/extensions-and-the-add-on-id/#when_do_you_need_an_add-on_id)

#### _*`manifest.json`*_
```json

    "browser_specific_settings": {
        "gecko": {
            "id": "your.app.name@your.domain.org"
        }
    }
}
```

This should go inside the overall manifest dict at the end.

Here's a complete working extension that has been tested on https://mozilla.org/: [librewolf-web-browser-extension-example](https://gitlab.com/tangibleai/librewolf-web-browser-extension-example/)
