# [40 Data Science Interview Questions](https://www.nicksingh.com/posts/40-probability-statistics-data-science-interview-questions-asked-by-fang-wall-street#section1)


1.  **Facebook - Easy\]** There is a fair coin (one side heads, one side tails) and an unfair coin (both sides tails). You pick one at random, flip it 5 times, and observe that it comes up as tails all five times. What is the chance that you are flipping the unfair coin?
2.  **\[Lyft - Easy\]** You and your friend are playing a game. The two of you will continue to toss a coin until the sequence HH or TH shows up. If HH shows up first, you win. If TH shows up first, your friend wins. What is the probability of you winning?
3.  **\[Google - Easy\]** What is the probability that a seven-game series goes to 7 games?
4.  **\[Facebook - Easy\]** Facebook has a content team that labels pieces of content on the platform as spam or not spam. 90% of them are diligent raters and will label 20% of the content as spam and 80% as non-spam. The remaining 10% are non-diligent raters and will label 0% of the content as spam and 100% as non-spam. Assume the pieces of content are labeled independently from one another, for every rater. Given that a rater has labeled 4 pieces of content as good, what is the probability that they are a diligent rater?
5.  **\[Bloomberg - Easy\]** Say you draw a circle and choose two chords at random. What is the probability that those chords will intersect?
6.  **\[Amazon - Easy\]** 1/1000 people have a particular disease, and there is a test that is 98% correct if you have the disease. If you don’t have the disease, there is a 1% error rate. If someone tests positive, what are the odds they have the disease?
7.  **\[Facebook - Easy\]** There are 50 cards of 5 different colors. Each color has cards numbered between 1 to 10. You pick 2 cards at random. What is the probability that they are not of same color and also not of same number?
8.  **\[Tesla - Easy\]** A fair six-sided die is rolled twice. What is the probability of getting 1 on the first roll and not getting 6 on the second roll?
9.  **\[Facebook - Easy\]** What is the expected number of rolls needed to see all 6 sides of a fair die?
10.  **\[Microsoft - Easy\]** Three friends in Seattle each told you it’s rainy, and each person has a 1/3 probability of lying. What is the probability that Seattle is rainy? Assume the probability of rain on any given day in Seattle is 0.25.
11.  **\[Uber - Easy\]** Say you roll three dice, one by one. What is the probability that you obtain 3 numbers in a strictly increasing order?
12.  **\[Bloomberg - Medium\]** Three ants are sitting at the corners of an equilateral triangle. Each ant randomly picks a direction and starts moving along the edge of the triangle. What is the probability that none of the ants collide? Now, what if it is k ants on all k corners of an equilateral polygon?
13.  **\[Two Sigma - Medium\]** What is the expected number of coin flips needed to get two consecutive heads?
14.  **\[Amazon - Medium\]** How many cards would you expect to draw from a standard deck before seeing the first ace?
15.  **\[Robinhood - Medium\]** A and B are playing a game where A has n+1 coins, B has n coins, and they each flip all of their coins. What is the probability that A will have more heads than B?
16.  **\[Airbnb - Medium\]** Say you are given an unfair coin, with an unknown bias towards heads or tails. How can you generate fair odds using this coin?
17.  **\[Quora - Medium\]** Say you have N i.i.d. draws of a normal distribution with parameters μ and σ. What is the probability that k of those draws are larger than some value Y?
18.  **\[Spotify - Hard\]** A fair die is rolled n times. What is the probability that the largest number rolled is r, for each r in 1..6?
19.  **\[Snapchat - Hard\]** There are two groups of n users, A and B, and each user in A is friends with those in B and vice versa. Each user in A will randomly choose a user in B as their best friend and each user in B will randomly choose a user in A as their best friend. If two people have chosen each other, they are mutual best friends. What is the probability that there will be no mutual best friendships?
20.  **\[Tesla - Hard\]** Suppose there is a new vehicle launch upcoming. Initial data suggests that any given day there is either a malfunction with some part of the vehicle or possibility of a crash, with probability p which then requires a replacement. Additionally, each vehicle that has been around for n days must be replaced. What is the long-term frequency of vehicle replacements?

## 20 Statistics Problems Asked By FANG & Hedge Funds

1.  **\[Facebook - Easy\]** How would you explain a confidence interval to a non-technical audience?
2.  **\[Two Sigma - Easy\]** Say you are running a multiple linear regression and believe there are several predictors that are correlated. How will the results of the regression be affected if they are indeed correlated? How would you deal with this problem?
3.  **\[Uber - Easy\]** Describe p-values in layman’s terms.
4.  **\[Facebook - Easy\]** How would you build and test a metric to compare two user’s ranked lists of movie/tv show preferences?
5.  **\[Microsoft - Easy\]** Explain the statistical background behind power.
6.  **\[Twitter - Easy\]** Describe A/B testing. What are some common pitfalls?
7.  **\[Google - Medium\]** How would you derive a confidence interval from a series of coin tosses?
8.  **\[Stripe - Medium\]** Say you model the lifetime for a set of customers using an exponential distribution with parameter λ, and you have the lifetime history (in months) of n customers. What is your best guess for λ?
9.  **\[Lyft - Medium\]** Derive the mean and variance of the uniform distribution U(a, b).
10.  **\[Google - Medium\]** Say we have X ~ Uniform(0, 1) and Y ~ Uniform(0, 1). What is the expected value of the minimum of X and Y?
11.  **\[Spotify - Medium\]** You sample from a uniform distribution \[0, d\] n times. What is your best estimate of d?
12.  **\[Quora - Medium\]** You are drawing from a normally distributed random variable X ~ N(0, 1) once a day. What is the approximate expected number of days until you get a value of more than 2?
13.  **\[Facebook - Medium\]** Derive the expectation for a geometric distributed random variable.
14.  **\[Google - Medium\]** A coin was flipped 1000 times, and 550 times it showed up heads. Do you think the coin is biased? Why or why not?
15.  **\[Robinhood - Medium\]** Say you have n integers 1…n and take a random permutation. For any integers i, j let a swap be defined as when the integer i is in the jth position, and vice versa. What is the expected value of the total number of swaps?
16.  **\[Uber - Hard\]** What is the difference between MLE and MAP? Describe it mathematically.
17.  **\[Google - Hard\]** Say you have two subsets of a dataset for which you know their means and standard deviations. How do you calculate the blended mean and standard deviation of the total dataset? Can you extend it to K subsets?
18.  **\[Lyft - Hard\]** How do you randomly sample a point uniformly from a circle with radius 1?
19.  **\[Two Sigma - Hard\]** Say you continually sample from some i.i.d. uniformly distributed (0, 1) random variables until the sum of the variables exceeds 1. How many times do you expect to sample?
20.  **\[Uber - Hard\]** Given a random Bernoulli trial generator, how do you return a value sampled from a normal distribution

## Solutions To Probability Interview Questions

**Problem #1 Solution:**

We can use Bayes Theorem here. Let U denote the case where we are flipping the unfair coin and F denote the case where we are flipping a fair coin. Since the coin is chosen randomly, we know that P(U) = P(F) = 0.5. Let 5T denote the event where we flip 5 heads in a row. Then we are interested in solving for P(U|5T), i.e., the probability that we are flipping the unfair coin, given that we saw 5 tails in a row.

We know P(5T|U) = 1 since by definition the unfair coin will always result in tails. Additionally, we know that P(5T|F) = 1/2^5 = 1/32 by definition of a fair coin. By Bayes Theorem we have:

