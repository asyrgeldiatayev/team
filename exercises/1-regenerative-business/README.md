# Regenerative Business

In this exercise you're going to reach out and give society a nudge!
And you'll get connected to a community you may not have heard about, others like you who are building and supporting prosocial businesses and ides.
Next San Diego (and other though leaders) call this "Regenerative Business".
So you're going to help them draft and edit a Wikipedia article that explains what regenerative business is.

1. Research "Regenerative Business" and decide what the term means to you: [my notes](./1-regenerative-business/prosocial-ai-regenerative-business-resources.md)
2. Create a wikipedia account
3. search for Regenerative Business to find the draft article

