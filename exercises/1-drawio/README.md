# DrawIO

Skip to  [Install](#install-drawio) if you already know what `drawio` is.

## Raster vs Vector diagrams

Raster images are the typical image files that you are used to, JPG and PNG are the most common formats.
PDFs are also sometimes just JPGs wrapped with some proprietary markup for Adobe Acrobat.

Vector graphics are diagrams that you can easily edit and reuse later.
And they are infinitely scalable.
You can zoom in or out and they still look sharp.
In a vector graphic image all objects are represented as vectors, kind-of like a flattened 3-D model of your image.
It's great for diagrams, because you can move around and zoom around in a 2-D vector diagram just like you can in a 3-D world created with Blender or a CAD tool.
Basically vector graphics is CAD for 2-D diagrams.

Draw IO is an open source Javascript application that lets you create these vector diagrams in `.drawio` files that you (or anyone else) can reuse later.
It's the only way to really open source your artwork or diagrams.
And it has a lot of boilerplate examples that can get you started on things like org charts, dialog trees, and flow diagrams.
There are even some open source cloud network architecture diagrams with icons for the big cloud services.

Visio and InksScape are comparable apps.
Inkscape can save and edit SVG files natively.
Visio has a proprietary format, so it can only export to SVG.
DrawIO's file format is open and just XML/text.
So it can even be automatically embedded in PNG files.
So your diagrams can be viewable by anyone anywhere and reusable by anyone with `drawio-desktop` installed.

If you just want to get started quickly, visit [diagrams.net](https://www.diagrams.net) and skip down to the [Example templates](#example-templates) section below [Installation](#installation).

## Installation

DrawIO is a javascript app, so it can run in your browser and there are paid services and free webapps that will let you creat diagrams without installing anything.
But it's much easier to work with `.drawio` files on your computer if you install the drawio-desktop app.

### Linux

```sh
sudo apt install -y wget curl
curl -s https://api.github.com/repos/jgraph/drawio-desktop/releases/latest | grep browser_download_url | grep '\.deb' | cut -d '"' -f 4 | wget -i -
sudo apt -f install ./drawio-amd64-*.deb
```

### Windows

You'll need to run this in a git-bash terminal for the automagic download to work:

```sh
curl -s https://api.github.com/repos/jgraph/drawio-desktop/releases/latest \
    | grep browser_download_url \
    | grep -E '"[^"]*\-windows\-installer\.exe"' \
    | cut -d '"' -f 4 \
    | wget -i -
chmod +x *-windows-installer.exe
./draw.io*-windows-installer.exe
```

If you're stuck on a machine without git-bash installed you will have to manually browse to the jgraph repo to find the binary install appropriate for your environment.

### Mac

```sh
sudo apt install -y wget curl
curl -s https://api.github.com/repos/jgraph/drawio-desktop/releases/latest | grep browser_download_url | grep '\.dmg"' | cut -d '"' -f 4 | wget -i -
```

Then manually launch the dmg (disk image) file and drag the DrawIO app into your Applications Folder (directory).

## Example templates

After installation the first thing you want to do is make sure you have some example templates to get you started.
You can find this under the **Extras -> Plugins** or  menu item or in the sidebar that pops out like a drawer in the left-hand margin.
Make sure you've checked the **View -> Search Shapes** menu item so you can search for shapes based on their name in the search box at the top of the left-hand sidebar.
The sidebar itself can be expanded or compressed by dragging the double-line handle on the border between your drawing and the sidebar.
You can even use the GUI elements you see in the DrawIO app itself to help you decide on widgets for your app.

The great thing about open source is how people cooperate to create examples and share those examples with the world.
Humans love to share (and show off) their creative work.
Even corporations have contributed vast libraries of examples drawio diagrams and clipart to help you get started (with their platforms).
Some organizations have even funded the creation of more generic diagram elements like UML and data flow diagram symbols.

Don't reinvent the wheel, build on the shoulders of giants... and the giant-sized generosity of open source contributors.
You can learn a lot by using someone else's example diagram.

Pay attention to:

- The kinds of drawing elements (lines, rectangles, ovals) in the example
- How elements are connected with lines
- Color schemes
- Open standards for the shapes and names of things

There are almost 50 different kinds of drawing elements in the "General" sidebar tab alone.
Do the diagrams you like use a lot of 3-D boxes and cylinders, or flat 2-D rectangles and ovals.
Are rounded corners more popular and pleasing than hard corners?
How rounded is rounded enough?
Are users and humans represented a stick figures or a cartoon "head shot"?

Highly reusable diagrams connect elements together with "connectors" which are permanently attached to connection points on a particular drawing element (usually a rectangular box).
Connector lines move and "reroute" themselves whenever you drag objects around.
And play with the various routing options for connector lines to see what works best for you.

Pay attention how the creator managed colors.
Which color schemes feel the most inviting and clear to you?
What about someone with low vision or color blindness?
This is a great opportunity to learn about accessibility and color blindness so you know which color combinations to avoid.
Most people who are colorblind see yellow and green as almost red, according to [this blog](https://www.medicinenet.com/what_colors_do_you_see_if_youre_color-blind/article.htm). 
So you want to use only one of those 3 colors in your color scheme.
And you can turn off green in your colors to see what it looks like to someone with this common type of colorblindness.

Pay attention to open standards diagrams and the shapes used to represent fundamental computer science concepts so you don't confuse your user by using the wrong shape for the wrong meaning or intent.


They spent a lot of time on these high quality diagram examples and you can learn a lot.

## Advanced

Once you've mastered the basics of getting around in drawio, the sky's the limit on what you can build and share with the world.

### Interactive wireframes

It's possible to create interactive wireframes with layers or pages.
You can assign a link to a page in your diagram for any element in your diagram.
This turns any element into an interactive button so that you can test the interactions in your wireframe by clicking on the element.
And it's even possible to create layers so that just a few elements appear and disappear based on button clicks within your app wireframe.
For more details, check out this [blog post](https://www.diagrams.net/blog/interactive-diagram-layers) on diagrams.net.

And if you want to share your interactive wireframe with people who do not yet have drawio isntalled, you can upload it to diagrams.net and use their _share_ feature to create a public permalink to your diagram.
You just send that link to your team and anyone can click on it to test your app wireframe and give you feedback on the UX.
