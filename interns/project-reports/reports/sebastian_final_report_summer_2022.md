# Using LLMs (Large Language Models) to paraphrase text
For my project for my summer 2022 internship I chose to pursue the subject of creating machine generated paraphrases.

## Presentation
### 1. the problem 
  - several right answers
  - The tower was very tall and located in Paris >>> The tower (located in Paris) is extremely tall
  - One challenge is creating a good metric because there are several right answers (talked about in the metric section)
### 2. The data
  - Input text                                    Target text
    The tower was very tall and located in Paris  The tower (located in Paris) is extremely tall
  - The dataset in made up of three datasets: Quora, Paws, ...
  - The dataset is ... examples long
  - There are ... duplicated
### 3. The metric
  - Bad paraphrases:
    - exact same
    - empty
    - too long
    - does not mean the same thing (negation)
  - Picking the worst score out of all of the true paraphrases
### 4. Edit distance
### 5. Cosine similarity
### 6. Slide for embedding diagram
### 7. Conclusion
  - Things learned
  - Next steps

## The Metric
In order in provide affective scoring of predicted paraphrases a custom metric has to be made.

Edit distance is the measure of how many operations (insertions and deletions) it takes for a certain text to match another text. An example would be "I am a dog person" and "I am a cat person". The edit distance would be 6 because is would take 6 insertions and deletions to make "I am a dog person" match "I am a cat person". the g is and replaced with c. The o is deleted and replaced with a, and so on.

This is important in measuring if a machine generated paraphrase is a true paraphrase because a good paraphrase should differ from the original text in a significant way and retain the same meaning as the original text. Of course this is hard to attain in practice, being something that can also be difficult for a human to perform.

Cosine distance is a method of finding if two texts retain the same meaning. First to find cosine distance cosine similarity be computed between the two embedding tensors. Cosine similarity is the opposite of cosine distance. Cosine similarity can be thought of as measing how much of two texts is alike and cosine distance can be thought of as measuring how much of two texts is not alike. To compute cosine similarity the two texts must be embedded/encoded into a vector representation of their meaning.

![embedding_diagram.png](./embedding_diagram.png)

Using a python library like `sentence-transformers` this can be achieved very easily. Simply instantiate one of their many embedding models suited for various different taks and measure the distace between the two tensors or arrays with their (`sentence_transformers`) built in function for this.

### Combination
Two combine these two elements, another method is added. Instead of just taking in two texts (one predicted by a model and one true human generated paraphrase), one model predicted paraphrase is given and a list of true human generated paraphrases is given.

The edit distance is computed for every true human generated paraphrase and the model's prediction, and the minimum is returned. This is because in order for the model to become more versatile returning different results everytime it helps to also score prediction with every prediction in the dataset. That way the model will learn to return different texts. By returning the minimum the model will get the score for the paraphrase it differs the most from and learn to create a prediction closer to that true paraphrase until its score on that paraphrase surpasses another paraphrase and the score for another pair will be returned. The same thing is computed for the cosine distance.

To finish it off the two scores are combined with a weighted average, the weights being passed in the class instantiation.

### Writing the metric
First create the class and define the `__init__` function:
```
class Metric():
  def __init__(self, embedding, cos_sim, edit_weight=0.3, min_edit_distance=5):
    self.embedding = embedding
    self.cos_sim = cos_sim
    self.edit_weight = edit_weight
    self.min_edit_distance = min_edit_distance
```
`embedding` is the embedding model, `cos_sim` is the function used to measure the cosine similarity between two embeddings, and `min_edit_distance` is the minimum edit distance that the two texts can have before the model is penalized.

The functions are named like so. If the function is a helper function to a `get_best_*` function it is named in a `get_* way` (the `*` should never be `best`). The next function is `get_min_edit_distance` which is used for subtracting an amount from the score if the edit distance is under a certain threshold.
```
  def get_min_edit_distance(self, sentences, min_edit_distance): 
    return max(.1 * min([len(s) for s in sentences]), min_edit_distance)
```
This function takes the all of the inputs to the metric and returns the max of ten percent of the minimum of the length of every sentence and the minimum edit distance.
The next part actually finds the edit distance between two texts:
```
  def edit_distance(self, input_sentence, output_sentence):
    if len(input_sentence) > len(output_sentence):
        input_sentence, output_sentence = input_sentence, output_sentence

    distances = range(len(input_sentence) + 1)
    for i2, c2 in enumerate(output_sentence):
        distances_ = [i2+1]
        for i1, c1 in enumerate(input_sentence):
            if c1 == c2:
                distances_.append(distances[i1])
            else:
                distances_.append(1 + min((distances[i1], distances[i1 + 1], distances_[-1])))
        distances = distances_
    return distances[-1] / max(len(input_sentence), len(output_sentence))
```
This next function finds the best cosine similarity between the models prediction and list of true paraphrases by taking the minimum:
```
  def get_best_cos_sim(self, text, other_texts):
    return min([self.get_cos_sim(text, s) for s in other_texts])
```
The next function takes a pair of texts and returns the cosine similarity:
```
  def get_cos_sim(self, text_a, text_b):
    return self.cos_sim(self.embedding.encode(text_a, convert_to_tensor=True), self.embedding.encode(text_b, convert_to_tensor=True))
```
This converts cosine similarity into cosine distance by taking 1 - the cosine similarity:
```
  def get_cos_dist(self, text_a, text_b):
    return 1 - self.get_cos_sim(text_a, text_b)
```
This does the same thing as `get_best_cos_sim` for for cosine distance:
```  
  def get_best_cos_dist(self, text, other_texts):
      return min([self.get_cos_dist(text, s) for s in other_texts])
```
Just like the above function this does the same thing as `get_best_cosine_sim` and `get_best_cos_dist` for for edit distance:
```
  def get_best_edit_distance(self, pred, true_paraphrases):
    return min([self.edit_distance(pred, truth) for truth in true_paraphrases])
```
Finaly the metric is computed using all of the functions defined above:
```
  def compute_metric(self, pred, true_paraphrases):
    min_edit_distance = self.get_min_edit_distance([pred] + list(true_paraphrases), self.min_edit_distance)
    edit_distance = self.get_best_edit_distance(pred, true_paraphrases)
    complete_edit_distance = np.abs(
            edit_distance
            - min_edit_distance
            ) / max([len(s) for s in true_paraphrases+[pred]])
    cos_dist = float(self.get_best_cos_dist(pred, true_paraphrases))
    return (complete_edit_distance * self.edit_weight) + (cos_dist * (1 - self.edit_weight))
```
First the minimum edit distance is found using the `get_min_edit_distance` function. Then the edit distance is computed with the `get_best_edit_distance`. Those two values are combined using `np.abs` and then are normalized.

## The Model
For actually generating the paraphrases I chose to finetune a BART model on a dataset created by combinding several pre existing datasets scraped from various websites. I chose to use BART because it can easily perform text2text generation (or conditional text generation).

* The dataset on Huggingface is here: https://huggingface.co/datasets/catasaurus/paraphrase-dataset2
* The gitlab repository is here: https://gitlab.com/tangibleai/paraphraser
* You can find the model on Huggingface here: https://huggingface.co/catasaurus/bart_paraphraser

### Using the Model
To use the model, simple use the code below and replace `s` with whatever text you want the bot to paraphrase.
```
from transformers import AutoNTokenizer, AutoModelForSeq2SeqLM
tokenizer = AutoTokenizer.from_pretrained("catasaurus/bart_paraphraser")
model = AutoModelForSeq2SeqLM.from_pretrained("catasaurus/bart_paraphraser")
s = "The structure is very tall, and located in Paris" # this can also be a list of strings
tokenized = tokenizer(s, return_tensors='pt')
pred = model.generate(tokenized['input_ids'])
decoded_pred = tokenizer.batch_decode(pred, skip_special_tokens=True)

print('\n'.join(s for s in decoded_pred)) # if a list of strings was inputed into the model this will print out every string in a newline
``` 
The code above first imports the necessary components of the Huggingface `transformers` library. The next two lines load the tokenizer and the model from Huggingface (they are kept there is git repositories). `tokenized` contains the tokenized sentence(s). `pred` generates the outputs from the model, but they are still kept in a tokenized representation. Finally `decoded_pred` decodes the tokenized outputs and the final line prints each of them out on a seperate line.

### Training the model
To train the model I used `simple-transformers`. The reason for me using this library instead of the plain Huggingface `transformers` library is because I was getting a lot of errors that did not really explain how they could be fixed are were not well documented. `simple-transformers` makes everything easier and allows you to finetune models with just a few lines of code most of which is configuring the hyper parameters (you also don't have to define a tokenizer, `simple-transformers` does it for you. All of that versus using `transformers` which in my opinion is not worth the hassle unless you want to use a differnet tokenizer? Or maybe have something a bit more well documented than `simple-transformers` which it makes up for with throwing very few errors and requiring very fiew lines of code.

### The dataset
The dataset that I started with for training the model was a combination of the Quora paraphrase dataset, PAWS, and {some other dataset I forgot the name}. I combined them (that just included chnanging the names of their columns so that they would match up with the other Pandas dataframes) and uploaded it to Huggingface.

### The Gitlab repository
The Gitlab repository is where all of the notebooks used for training the model, creating the dataset(s), plotting the model's performance and various other things are stored including the metric.

## Next steps
