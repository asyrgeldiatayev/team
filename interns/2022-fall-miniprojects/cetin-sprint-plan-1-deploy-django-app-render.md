# Sprint Plan

## Deploy a django app to render.com

- [X] C8: translate simple version of "book journal" app to Django (sqlite)
- [ ] C8: work through the Django tutorial: https://docs.djangoproject.com/en/4.1/intro/tutorial01/
- [X] OR find Corey Shafer's video series and build the app that he creates in the tutoriL; https://www.youtube.com/playlist?list=PL-osiE80TeTtoQCKZ03TU5fNfx2UY6U4p

- [ ] create an account on Render.com
- [ ] deploy your app to render.com using their "free" tier
- [ ] Hobson will add your e-mail to our Render account
- [ ] deploy your app to our paid account (faster/bigger server)
